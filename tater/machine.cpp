#include <tater/machine.hpp>
#include <tater/function.hpp>

#include <iostream>

namespace tater
{
  Machine::Machine(Function& f)
    : m_fn(&f), m_pc(0), m_halted(false)
  {
    // Load global symbols into program memory.
    m_mem.push_back(Value(m_fn));
  }

  void
  Machine::run()
  {
    while (is_running())
      next();
  }

  void
  Machine::next()
  {
    assert(is_running());
    decode(fetch());  
  }

  Value
  Machine::top() const
  {
    assert(!m_ops.empty());
    return m_ops.top();
  }

  void
  Machine::push(Value v)
  {
    m_ops.push(v);
  }

  Value
  Machine::pop()
  {
    assert(!m_ops.empty());
    Value v = top();
    m_ops.pop();
    return v;
  }

  // TODO: Can we add an indeterminate state to objects in order to
  // detect reads before initializations?
  static Value
  make_value(Type t)
  {
    switch (t) {
    case int_type:
      return Value(Int_value());
    case float_type:
      return Value(Float_value());
    case addr_type:
      return Value(Addr_value());
    case fn_type:
      return Value(Fn_value());
    }
  }

  Value
  Machine::allocate(Type t)
  {
    m_mem.push_back(make_value(t));
    return Value(Int_value(m_mem.size() - 1));
  }

  Value
  Machine::load(Type t, Value addr)
  {
    Addr_value a = addr.get_address();
    assert(a < m_mem.size());
    Value& v = m_mem[a];
    assert(v.get_type() == t);
    return v;
  }

  void
  Machine::store(Value addr, Value val)
  {
    Addr_value a = addr.get_address();
    assert(a < m_mem.size());
    Value& v = m_mem[a];
    assert(v.get_type() == val.get_type());
    v = val;
  }

  static void
  not_implemented()
  {
    throw std::logic_error("not implemented");
  }

  Instruction*
  Machine::fetch()
  {
    Instruction* insn = m_fn->get_instruction(m_pc++);
    std::cout << m_pc << ' ' << insn->get_kind() << '\n';
    return insn;
  }

  void
  Machine::decode(Instruction* insn)
  {
    switch (insn->get_kind()) {
    // Integer instructions
    case Instruction::push_insn:
      return push(static_cast<Push_instruction*>(insn));
    case Instruction::pop_insn:
      return pop(static_cast<Pop_instruction*>(insn));
    case Instruction::copy_insn:
      return copy(static_cast<Copy_instruction*>(insn));
    case Instruction::swap_insn:
      return swap(static_cast<Swap_instruction*>(insn));
    case Instruction::add_insn:
      return add(static_cast<Add_instruction*>(insn));
    case Instruction::sub_insn:
      return sub(static_cast<Sub_instruction*>(insn));
    case Instruction::mul_insn:
      return mul(static_cast<Mul_instruction*>(insn));
    case Instruction::div_insn:
      return div(static_cast<Div_instruction*>(insn));
    case Instruction::rem_insn:
      return rem(static_cast<Rem_instruction*>(insn));
    case Instruction::cmp_insn:
      return cmp(static_cast<Cmp_instruction*>(insn));

    // Floating point instructions
    case Instruction::fpush_insn:
    case Instruction::fpop_insn:
    case Instruction::fcopy_insn:
    case Instruction::fswap_insn:
    case Instruction::fadd_insn:
    case Instruction::fsub_insn:
    case Instruction::fmul_insn:
    case Instruction::fdiv_insn:
    case Instruction::frem_insn:
    case Instruction::fcmp_insn:
      return not_implemented();

    // Address instructions
    case Instruction::alloca_insn:
      return allocate(static_cast<Alloca_instruction*>(insn));
    case Instruction::load_insn:
      return load(static_cast<Load_instruction*>(insn));
    case Instruction::store_insn:
      return store(static_cast<Store_instruction*>(insn));

    // Branching instructions
    case Instruction::bif_insn:
    case Instruction::br_insn:    
      return not_implemented();
    
    // Program execution
    case Instruction::halt_insn:    
      return halt(static_cast<Halt_instruction*>(insn));
    }
  }

  void
  Machine::push(Push_instruction* insn)
  {
    push(insn->get_value());
  }
  
  void
  Machine::pop(Pop_instruction* insn)
  {
    pop();
  }
  
  void
  Machine::copy(Copy_instruction* insn)
  {
    push(top());
  }
  
  void
  Machine::swap(Swap_instruction* insn)
  {
    Value v1 = pop();
    Value v2 = pop();
    push(v2);
    push(v1);
  }
  
  void
  Machine::add(Add_instruction* insn)
  {
    // FIXME: Detect overflow.
    Value v1 = pop();
    Value v2 = pop();
    Value v(v1.get_integer() + v2.get_integer());
    push(v);
  }
  
  void
  Machine::sub(Sub_instruction* insn)
  {
    // FIXME: Detect overflow.
    Value v1 = pop();
    Value v2 = pop();
    Value v(v1.get_integer() - v2.get_integer());
    push(v);
  }
  
  void
  Machine::mul(Mul_instruction* insn)
  {
    // FIXME: Detect overflow.
    Value v1 = pop();
    Value v2 = pop();
    Value v(v1.get_integer() * v2.get_integer());
    push(v);
  }
  
  void
  Machine::div(Div_instruction* insn)
  {
    // FIXME: Detect overflow.
    // FIXME: Detect division by 0.
    Value v1 = pop();
    Value v2 = pop();
    Value v(v1.get_integer() / v2.get_integer());
    push(v);
  }
  
  void
  Machine::rem(Rem_instruction* insn)
  {
    // FIXME: Detect overflow.
    // FIXME: Detect division by 0.
    Value v1 = pop();
    Value v2 = pop();
    Value v(v1.get_integer() % v2.get_integer());
    push(v);
  }

  static Value
  compare_eq(Value v1, Value v2)
  {
    return Value(Int_value(v1.get_integer() == v2.get_integer()));
  }

  static Value
  compare_ne(Value v1, Value v2)
  {
    return Value(Int_value(v1.get_integer() != v2.get_integer()));
  }

  static Value
  compare_slt(Value v1, Value v2)
  {
    return Value(Int_value(v1.get_signed() < v2.get_signed()));
  }

  static Value
  compare_sgt(Value v1, Value v2)
  {
    return Value(Int_value(v1.get_signed() > v2.get_signed()));
  }

  static Value
  compare_sle(Value v1, Value v2)
  {
    return Value(Int_value(v1.get_signed() <= v2.get_signed()));
  }

  static Value
  compare_sge(Value v1, Value v2)
  {
    return Value(Int_value(v1.get_signed() >= v2.get_signed()));
  }

  static Value
  compare_ult(Value v1, Value v2)
  {
    return Value(Int_value(v1.get_unsigned() < v2.get_unsigned()));
  }

  static Value
  compare_ugt(Value v1, Value v2)
  {
    return Value(Int_value(v1.get_unsigned() > v2.get_unsigned()));
  }

  static Value
  compare_ule(Value v1, Value v2)
  {
    return Value(Int_value(v1.get_unsigned() <= v2.get_unsigned()));
  }

  static Value
  compare_uge(Value v1, Value v2)
  {
    return Value(Int_value(v1.get_unsigned() >= v2.get_unsigned()));
  }


  void
  Machine::cmp(Cmp_instruction* insn)
  {
    Value v1 = pop();
    Value v2 = pop();
    switch (insn->get_operator()) {
    case Instruction::eq:
      return push(compare_eq(v1, v2));
    case Instruction::ne:
      return push(compare_ne(v1, v2));
    case Instruction::slt:
      return push(compare_slt(v1, v2));
    case Instruction::ult:
      return push(compare_ult(v1, v2));
    case Instruction::sgt:
      return push(compare_sgt(v1, v2));
    case Instruction::ugt:
      return push(compare_ugt(v1, v2));
    case Instruction::sle:
      return push(compare_sle(v1, v2));
    case Instruction::ule:
      return push(compare_ule(v1, v2));
    case Instruction::sge:
      return push(compare_sge(v1, v2));
    case Instruction::uge:
      return push(compare_uge(v1, v2));
    }
  }
  
  void
  Machine::allocate(Alloca_instruction* insn)
  {
    Value val = allocate(insn->get_type());
    push(val);
  }
  
  void
  Machine::load(Load_instruction* insn)
  {
    Value addr = pop();
    Value val = load(insn->get_type(), addr);
    push(val);
  }
  
  void
  Machine::store(Store_instruction* insn)
  {
    Value addr = pop();
    Value val = pop();
    store(addr, val);
    push(addr);
  }

  void
  Machine::halt(Halt_instruction* insn)
  {
    assert(is_running());
    m_halted = true;
  }

} // namespace tater
