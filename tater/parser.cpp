#include <tater/parser.hpp>
#include <tater/function.hpp>
#include <tater/instruction.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>

namespace tater
{
  static std::string
  read_file(std::istream& is)
  {
    using Iter = std::istreambuf_iterator<char>;
    return std::string(Iter(is), Iter());
  }

  static std::string
  read_file(std::string const& path)
  {
    std::ifstream ifs(path);
    return read_file(ifs);
  }

  Parser::Parser(std::string const& path)
    : m_path(path), m_text(read_file(path))
  {
    init();
  }

  Parser::Parser(std::istream& is)
    : m_path(), m_text(read_file(is))
  {
    init();
  }

  void
  Parser::init()
  {
    // Stack operations.
    m_parsers["push"] = &Parser::parse_push_instruction;
    m_parsers["pop"] = &Parser::parse_pop_instruction;
    
    // Integer operations.
    m_parsers["add"] = &Parser::parse_add_instruction;
    m_parsers["mul"] = &Parser::parse_mul_instruction;

    // Program execution
    m_parsers["halt"] = &Parser::parse_halt_instruction;

    // FXIME: Add more parsers.

    // Initialize the type table
    m_types["int"] = int_type;
    m_types["float"] = float_type;
    m_types["addr"] = addr_type;
    m_types["fn"] = fn_type;
  }

  Function*
  Parser::parse_function()
  {
    // FIXME: Stop leaking memory.
    m_fn = new Function;

    std::istringstream ss(m_text);
    for (std::string line; std::getline(ss, line); ) {
      if (line.empty())
        continue;
      parse_line(line);
    }

    return m_fn;
  }

  void
  Parser::parse_line(std::string const& str)
  {
    std::size_t n = str.find_first_of(' ');
    std::string op(str, 0, n);
    std::string rest;
    if (n != std::string::npos)
      rest = std::string(str, n + 1, str.size() - n);

    auto iter = m_parsers.find(op);
    if (iter == m_parsers.end()) {
      std::stringstream ss;
      ss << "unknown opcode '" << op << "'";
      throw std::runtime_error(ss.str());
    }
    auto parse = iter->second;

    // Invoke the parser.
    (this->*parse)(rest);
  }

  template<typename T, typename... Args>
  void 
  make_insn(Function* fn, Args&&... args)
  {
    Instruction* insn = new T(std::forward<Args>(args)...);
    fn->add_instruction(insn);
  }

  /// push n
  void
  Parser::parse_push_instruction(std::string const& rest)
  {
    char* end;
    Int_value n = std::strtol(rest.c_str(), &end, 10);
    make_insn<Push_instruction>(m_fn, Value(n));
  }

  /// pop t
  void
  Parser::parse_pop_instruction(std::string const& rest)
  {
    auto iter = m_types.find(rest);
    if (iter == m_types.end())
      throw std::runtime_error("invalid type");
    make_insn<Pop_instruction>(m_fn, iter->second);
  }

  /// add
  void
  Parser::parse_add_instruction(std::string const& rest)
  {
    make_insn<Add_instruction>(m_fn);
  }

  /// mul
  void
  Parser::parse_mul_instruction(std::string const& rest)
  {
    make_insn<Mul_instruction>(m_fn);
  }

  /// halt
  void
  Parser::parse_halt_instruction(std::string const& rest)
  {
    make_insn<Halt_instruction>(m_fn);
  }

} // namespace tater
