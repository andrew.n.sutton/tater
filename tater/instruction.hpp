#pragma once

#include <tater/type.hpp>
#include <tater/value.hpp>
#include <tater/label.hpp>

namespace tater
{
  /// Represents the set of all TaterVM instructions.
  ///
  /// \todo When we have symbols, we'll want memory functions that can
  /// push the address of functions and global variables. Maybe call
  /// that a "get" instruction?
  ///
  /// \todo Differentiate between signed and unsigned division and remainder.
  class Instruction
  {
  public:
    enum Kind
    {
      // Stack instructions
      push_insn,
      pop_insn,
      copy_insn,
      swap_insn,

      // Integer instructions
      add_insn,
      sub_insn,
      mul_insn,
      div_insn,
      rem_insn,
      cmp_insn,

      // Floating point instructions
      fpush_insn,
      fpop_insn,
      fcopy_insn,
      fswap_insn,
      fadd_insn,
      fsub_insn,
      fmul_insn,
      fdiv_insn,
      frem_insn,
      fcmp_insn,

      // Address instructions
      alloca_insn,
      load_insn,
      store_insn,

      // Branching instructions
      bif_insn,
      br_insn,

      // Program execution
      halt_insn,
    };

    /// Integer comparison operators.
    enum Operator
    {
      eq, // Equal to
      ne, // Not equal to
      slt, // Signed less than
      ult, // Unsigned less than
      sgt, // Signed greater than
      ugt, // Unsigned greater than
      sle, // Signed less than or equal to
      ule, // Unsigned less than or equal to
      sge, // Signed greater than or equal to
      uge, // Unsigned greater than or equal to
    };

  protected:
    Instruction(Kind k);

  public:
    Kind get_kind() const { return m_kind; }
    /// Returns the kind of the instruction.

  private:
    Kind m_kind;
  };

  inline
  Instruction::Instruction(Kind k)
    : m_kind(k)
  { }


  /// The base class of all explicitly typed instructions. 
  class Typed_instruction : public Instruction
  {
  protected:
    Typed_instruction(Kind k, Type t);

  public:
    Type get_type() const { return m_type; }
    /// Returns the type of object allocated.

  private:
    Type m_type;
  };


  /// Represents the instruction `push n` where `n` is an integer literal.
  ///
  /// Push an integer value onto the operand stack.
  class Push_instruction : public Instruction
  {
  public:
    Push_instruction(Value n);
    /// Constructs the instruction `push n`.

    Value get_value() const { return m_value; }
    /// Returns the value to be pushed.
    
  private:
    Value m_value;
  };

  inline
  Push_instruction::Push_instruction(Value n)
    : Instruction(push_insn), m_value(n)
  {
    assert(m_value.is_integer());
  }


  /// Represents the instruction `pop t`.
  ///
  /// Pop a value of type `t` off the operand stack.
  class Pop_instruction : public Typed_instruction
  {
  public:
    Pop_instruction(Type t);
    /// Constructs the instruction `pop t`.
  };

  inline
  Pop_instruction::Pop_instruction(Type t)
    : Typed_instruction(pop_insn, t)
  { }


  /// Represents the instruction `copy`.
  ///
  /// Copy the top operand on the stack. 
  class Copy_instruction : public Instruction
  {
  public:
    Copy_instruction();
    /// Constructs the instruction `copy`.
  };

  inline
  Copy_instruction::Copy_instruction()
    : Instruction(copy_insn)
  { }


  /// Represents the instruction `swap`.
  ///
  /// Swap the top two operands on the stack. The operands must have the same
  /// type.
  class Swap_instruction : public Instruction
  {
  public:
    Swap_instruction();
    /// Constructs the instruction `swap`.
  };

  inline
  Swap_instruction::Swap_instruction()
    : Instruction(swap_insn)
  { }


  /// Represents the instruction `add`.
  ///
  /// Add the top two operands, pushing the result.
  class Add_instruction : public Instruction
  {
  public:
    Add_instruction();
    /// Constructs the instruction `add`.
  };

  inline
  Add_instruction::Add_instruction()
    : Instruction(add_insn)
  { }


  /// Represents the instruction `sub`.
  ///
  /// Subtract the top operand from the lower and push the result.
  class Sub_instruction : public Instruction
  {
  public:
    Sub_instruction();
    /// Constructs the instruction `sub`.
  };

  inline
  Sub_instruction::Sub_instruction()
    : Instruction(sub_insn)
  { }


  /// Represents the instruction `mul`.
  ///
  /// Multiply the top two operands and push the result.
  class Mul_instruction : public Instruction
  {
  public:
    Mul_instruction();
    /// Constructs the instruction `mul`.
  };

  inline
  Mul_instruction::Mul_instruction()
    : Instruction(mul_insn)
  { }


  /// Represents the instruction `div`.
  ///
  /// Divide the top two operand by the lower and push the quotient.
  class Div_instruction : public Instruction
  {
  public:
    Div_instruction();
    /// Constructs the instruction `div`.
  };

  inline
  Div_instruction::Div_instruction()
    : Instruction(div_insn)
  { }


  /// Represents the instruction `rem`.
  ///
  /// Divide the top two operand by the lower and push the remainder.
  class Rem_instruction : public Instruction
  {
  public:
    Rem_instruction();
    /// Constructs the instruction `rem`.
  };

  inline
  Rem_instruction::Rem_instruction()
    : Instruction(rem_insn)
  { }


  /// Represents the instruction `cmp op` where `op` is a relation operator.
  ///
  /// Compare the top operands using the given operation and push the 
  /// result.
  class Cmp_instruction : public Instruction
  {
  public:
    Cmp_instruction(Operator op);
    /// Constructs the instruction `cmp op`.

    Operator get_operator() const { return m_op; }
    /// Returns the comparison operator.

  private:
    Operator m_op;
  };

  inline
  Cmp_instruction::Cmp_instruction(Operator op)
    : Instruction(cmp_insn), m_op(op)
  { }


  /// Represents the instruction `bif t f` where `t` and `f` are labels.
  ///
  /// If the top operand is true, jump to the label `t`, otherwise jump
  /// to the label `f`.
  ///
  /// \todo How should we represent labels?
  class Bif_instruction : public Instruction
  {
  public:
    Bif_instruction(Label t, Label l);
    /// Constructs the instruction `bif t l`.

    Label get_true_label() const { return m_true; }
    /// Returns the true label.

    Label get_false_label() const { return m_false; }
    /// Returns the false label.

  private:
    Label m_true;
    Label m_false;
  };

  inline
  Bif_instruction::Bif_instruction(Label t, Label l)
    : Instruction(bif_insn), m_true(t), m_false(l)
  { }


  /// Represents the instruction `br l` where `l` is a label.
  ///
  /// Jumps to the indicated label.
  class Br_instruction : public Instruction
  {
  public:
    Br_instruction(Label l);
    /// Constructs the instruction `br l`.

    Label get_label() const { return m_dest; }
    /// Returns the destination label.

  private:
    Label m_dest;
  };

  inline
  Br_instruction::Br_instruction(Label l)
    : Instruction(br_insn), m_dest(l)
  { }

  inline
  Typed_instruction::Typed_instruction(Kind k, Type t)
    : Instruction(k), m_type(t)
  { }


  /// Represents the instruction `alloca t` where `t` is a type of value.
  ///
  /// Adds a new object to the program stack, pushing the address of the
  /// allocated object.
  class Alloca_instruction : public Typed_instruction
  {
  public:
    Alloca_instruction(Type t);
    /// Constructs the instruction `alloca t`.
  };

  inline
  Alloca_instruction::Alloca_instruction(Type t)
    : Typed_instruction(alloca_insn, t)
  { }


  /// Represents the instruction `load t`.
  ///
  /// Push the value of the top operand.
  class Load_instruction : public Typed_instruction
  {
  public:
    Load_instruction(Type t);
    /// Constructs the instruction `load t`.
  };

  inline
  Load_instruction::Load_instruction(Type t)
    : Typed_instruction(load_insn, t)
  { }


  /// Represents the instruction `store`.
  ///
  /// Push the value of the top operand into the object referenced by the
  /// lower operand. Pushes the address of the object.
  ///
  /// \todo Do we need to specify the type of object stored?
  class Store_instruction : public Instruction
  {
  public:
    Store_instruction();
    /// Constructs the instruction `store t`.
  };

  inline
  Store_instruction::Store_instruction()
    : Instruction(store_insn)
  { }


  /// Represents the instruction `push n` where `n` is an integer literal.
  ///
  /// Push an integer value onto the operand stack.
  class Halt_instruction : public Instruction
  {
  public:
    Halt_instruction();
    /// Constructs the instruction `push n`.
  };

  inline
  Halt_instruction::Halt_instruction()
    : Instruction(halt_insn)
  { }

} // namespace tater
