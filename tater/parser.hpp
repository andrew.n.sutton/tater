#pragma once

#include <tater/type.hpp>

#include <iosfwd>
#include <string>
#include <unordered_map>

namespace tater
{
  class Function;
  class Instruction;

  /// The parser is responsible for returning a program (or module?) from
  /// an input file.
  ///
  /// \todo Stop using strings to manage parsing and build a real parser.
  /// This is way to fragile in its current form.
  class Parser
  {
  public:
    Parser(std::string const& path);
    /// Construct a parser for the given file.

    Parser(std::istream& is);
    /// Construct a parser for the given input stream.

    Function* parse_function();
    /// Parse a function definition. For now, this is simply an unnamed
    /// list of instructions. Obviously, we'd like to  make this more
    /// meaningful.

  private:
    void parse_line(std::string const& line);
    /// Parse a line of text.

    // Instruction parsers
    
    void parse_push_instruction(std::string const& rest);
    /// Parses a push instruction.

    void parse_pop_instruction(std::string const& rest);
    /// Parses a pop instruction.

    void parse_add_instruction(std::string const& rest);
    /// Parses a add instruction.

    void parse_mul_instruction(std::string const& rest);
    /// Parses a mul instruction.

    void parse_halt_instruction(std::string const& rest);
    // Parse a halt instruction.

    void init();
    /// Perform common initialization.

  private:
    std::string m_path;
    /// The path to the file.

    std::string m_text;
    /// The text of the file.

    Function* m_fn;
    /// The current function.

    using Parse_fn = void (Parser::*)(std::string const&);
    /// A function that parses the remainder of a string.

    std::unordered_map<std::string, Parse_fn> m_parsers;
    /// Associates each opcode and directive with a parser for its
    /// remaining text.

    std::unordered_map<std::string, Type> m_types;
  };

} // namespace tater
