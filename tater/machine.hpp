#pragma once

#include <tater/value.hpp>
#include <tater/instruction.hpp>

#include <vector>
#include <stack>

namespace tater
{
  /// Represents the virtual machine.
  ///
  /// \todo The VM should take a program as input or as an operand to
  /// `run`. A program is comprised of a list of functions global values,
  /// with one entry point (`main`).
  class Machine
  {
  public:
    Machine(Function& f);

    // Accessors

    std::stack<Value> const& get_operand_stack() const { return m_ops; }
    /// Return the operand stack.

    // Program execution

    void run();
    /// Execute the active function.

    void next();
    /// Executes one instruction.

    bool is_running() const { return !m_halted; }
    /// Returns true if the program is running.

    bool is_halted() const { return m_halted; }
    /// Returns true if the program has halted.

  private:
    // Operand stack.
    
    Value top() const;
    /// Returns the value at the top of  the stack.

    void push(Value);
    /// Push the value onto the stack.

    Value pop();
    /// Pops the top value, returning it.

    // Program stack

    Value allocate(Type t);
    /// Allocate a local object of type t.

    Value load(Type t, Value addr);
    /// Load the value of an object having type t.

    void store(Value addr, Value val);
    /// Store `val` into `addr`.

    // Instruction processing

    Instruction* fetch();
    /// Fetch the next instruction and update the program counter.
    
    void decode(Instruction* insn);
    /// Dispatch the instruction.

    void push(Push_instruction* insn);
    /// Push a value onto the operand stack.
    
    void pop(Pop_instruction* insn);
    /// Pop a value from the operand stack.
    
    void copy(Copy_instruction* insn);
    /// Copy the top operand.
    
    void swap(Swap_instruction* insn);
    /// Swap the top operand with the lower operand.
    
    void add(Add_instruction* insn);
    /// Add the top operands and push the result.
    
    void sub(Sub_instruction* insn);
    /// Subtract the top operand from the lower operand and push the result.
    
    void mul(Mul_instruction* insn);
    /// Multiply the top operands and push the result.
    
    void div(Div_instruction* insn);
    /// Divide the top operand by the lower and operand and push the quotient.
    
    void rem(Rem_instruction* insn);
    /// Divide the top operand by the lower and operand and push the remainder.

    void cmp(Cmp_instruction* insn);
    /// Compare the top operands and push the result.
    
    void allocate(Alloca_instruction* insn);
    /// Allocate a local object.
    
    void load(Load_instruction* insn);
    /// Load the address and push the value there.
    
    void store(Store_instruction* insn);
    /// Store the top value into the lower address and push the address.

    void halt(Halt_instruction* insn);
    /// Update the state of the machine to halted.

  private:
    Function* m_fn;
    /// The active function.

    unsigned m_pc;
    /// The program counter.

    bool m_halted;
    /// True if a halt instruction has been encountered.

    std::stack<Value> m_ops;
    /// The operand stack.

    std::vector<Value> m_mem;
    /// The program stack.
    ///
    /// \todo This could be a segmented data structure so that we can
    /// detect invalid reads and writes.
  };

} // namespace tater
