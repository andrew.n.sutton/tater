#include <tater/value.hpp>

namespace tater
{
  std::ostream& 
  operator<<(std::ostream& os, Value const& v)
  {
    switch (v.get_type()) {
    case int_type:
      return os << v.get_integer();
    case float_type:
      return os << v.get_float();
    case addr_type:
      return os << v.get_address();
    case fn_type:
      return os << v.get_function();
    }
    assert(false);
  }


} // namespace tater
