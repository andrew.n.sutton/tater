#pragma once

#include <tater/instruction.hpp>

#include <vector>

namespace tater
{
  /// Represents a single function. A function is primarily defined by
  /// a list of instructions.
  ///
  /// \todo Functions also have names and types. And are really comprised
  /// of sequences of basic blocks (where each block is a list of 
  /// instructions).
  ///
  /// \todo This should really be a type of value so that we can place
  /// it into program storage.
  ///
  /// \todo This is a terrible interface.
  class Function : public std::vector<Instruction*>
  {
  public:
    using std::vector<Instruction*>::vector;
    // Inherit all of vector's constructors.

    Instruction* get_instruction(unsigned n) const { return (*this)[n]; }
    /// Returns the instruction at address `n` within this function.

    void add_instruction(Instruction* insn) { push_back(insn); }
    /// Adds an instruction to the function.
  };

} // namespace tater
