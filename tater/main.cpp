#include <tater/function.hpp>
#include <tater/machine.hpp>
#include <tater/parser.hpp>

#include <iostream>

using namespace tater;

int 
main(int argc, char* argv[])
{
  // Parse a function definition from input.
  Parser parse(argv[1]);
  Function* fn = parse.parse_function();

  Machine m(*fn);
  m.run();

  // Print the last value on the operand stack.
  std::cout << "output == " << m.get_operand_stack().top() << '\n';


  #if 0
  // Build a function that computes the value of 3 + (2 * 4).
  //
  // push 3
  // push 2
  // push 4
  // mul
  // add
  // halt
  Function fn {
    new Push_instruction(Value(3)),
    new Push_instruction(Value(2)),
    new Push_instruction(Value(4)),
    new Mul_instruction(),
    new Add_instruction(),
    new Halt_instruction(),
  };

  Machine m(fn);
  m.run();

  #endif

}
